package com.crgp.platform.gui.panel;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import com.crgp.platform.gui.MainWindow;
import com.crgp.platform.xml.XMLReader;
import com.crgp.platform.xml.bean.RobotBean;

/**
 * 机器人列表
 * 
 * @author jhs
 * 
 */
public class RobotListPanel extends JPanel {

	private static final long serialVersionUID = -478019699541796298L;

	private final int ROBOT_LIST_TOOL_HEIGHT = 50;

	private JList<RobotBean> allRobotList;
	private JList<RobotBean> selectRobotList;

	private JPanel selectRobotListTool;

	private JButton upButton;
	private JButton downButton;
	private JButton addPlayerButton;

	private int x;
	private int y;
	private int width;
	private int height;

	private MainWindow main;

	public RobotListPanel(MainWindow main, int x, int y, int width, int height) {
		this.main = main;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;

		initUI();
		initEvent();
	}

	private void initEvent() {
		getAllList().addMouseListener(new AllListMouseListener());
		getSelectList().addMouseListener(new SelectedListMouseListener());
		getSelectList().getModel().addListDataListener(
				new SelectedListDataListener());
	}

	private void initUI() {
		setBorder(new LineBorder(Color.gray));
		setBounds(x, y, width, height);
		setLayout(null);

		add(getAllList());
		add(getSelectList());
		add(getToolBar());
	}

	public JList<RobotBean> getAllList() {
		if (allRobotList == null) {
			DefaultListModel<RobotBean> model = new DefaultListModel<RobotBean>();
			try {
				RobotBean[] robots = XMLReader.readRobotList();
				for (RobotBean robot : robots) {
					model.addElement(robot);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			allRobotList = new JList<RobotBean>(model);
			allRobotList.setBorder(new LineBorder(Color.gray));
			allRobotList.setBounds(0, 0, width, height / 2);
		}
		return allRobotList;
	}

	public JList<RobotBean> getSelectList() {
		if (selectRobotList == null) {
			selectRobotList = new JList<RobotBean>(
					new DefaultListModel<RobotBean>());
			selectRobotList.setBorder(new LineBorder(Color.gray));
			selectRobotList.setBounds(0, height / 2, width, height / 2
					- ROBOT_LIST_TOOL_HEIGHT);
		}
		return selectRobotList;
	}

	public JPanel getToolBar() {
		if (selectRobotListTool == null) {
			selectRobotListTool = new JPanel();
			selectRobotListTool.setLayout(new FlowLayout(FlowLayout.CENTER));
			selectRobotListTool.setBounds(0, height - ROBOT_LIST_TOOL_HEIGHT,
					width, ROBOT_LIST_TOOL_HEIGHT);

			selectRobotListTool.add(getUpButton());
			selectRobotListTool.add(getDownButton());
			selectRobotListTool.add(getAddPlayerButton());
		}
		return selectRobotListTool;
	}

	public JButton getUpButton() {
		if (upButton == null) {
			upButton = new JButton("∧");
		}

		return upButton;
	}

	public JButton getDownButton() {
		if (downButton == null) {
			downButton = new JButton("∨");
		}
		return downButton;
	}

	public JButton getAddPlayerButton() {
		if (addPlayerButton == null) {
			addPlayerButton = new JButton("+");

			addPlayerButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					if (getSelectList().getModel().getSize() < main
							.getPlayerCount()) {
						((DefaultListModel<RobotBean>) getSelectList()
								.getModel()).addElement(new RobotBean(
								RobotBean.PLAYER_ROBOT_UID));
					}
				}
			});
		}
		return addPlayerButton;
	}

	class AllListMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent event) {
			if (event.getClickCount() == 2
					&& event.getButton() == MouseEvent.BUTTON1) {
				RobotBean selectItem = allRobotList.getSelectedValue();
				if (selectItem != null) {
					if (getSelectList().getModel().getSize() < main
							.getPlayerCount()) {
						((DefaultListModel<RobotBean>) getSelectList()
								.getModel()).addElement(selectItem);
					}
				}
			}
		}
	}

	class SelectedListMouseListener extends MouseAdapter {

		@Override
		public void mouseClicked(MouseEvent event) {
			if (event.getClickCount() == 2
					&& event.getButton() == MouseEvent.BUTTON1) {
				RobotBean selectItem = selectRobotList.getSelectedValue();
				if (selectItem != null) {
					if (selectItem instanceof RobotBean) {
						((DefaultListModel<RobotBean>) getSelectList()
								.getModel()).removeElement(selectItem);
					} else {
						((DefaultListModel<RobotBean>) getSelectList()
								.getModel()).removeElement(selectItem);
					}
				}
			}
		}
	}

	class SelectedListDataListener implements ListDataListener {

		@Override
		public void intervalRemoved(ListDataEvent arg0) {
			if (getSelectList().getModel().getSize() < main.getPlayerCount()) {
				main.setLauncherEnable(false);
			}
		}

		@Override
		public void intervalAdded(ListDataEvent arg0) {
			if (getSelectList().getModel().getSize() == main.getPlayerCount()) {
				main.setLauncherEnable(true);
			}
		}

		@Override
		public void contentsChanged(ListDataEvent arg0) {

		}

	}
}
