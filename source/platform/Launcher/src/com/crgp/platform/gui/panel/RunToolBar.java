package com.crgp.platform.gui.panel;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import com.crgp.platform.gui.MainWindow;

public class RunToolBar extends JPanel {
	private static final long serialVersionUID = -1931574289481119903L;

	/**
	 * 准备
	 */
	private JButton readyButton;
	/**
	 * 暂停、继续
	 */
	private JButton pauseAndResumeButton;
	/**
	 * 开始、停止
	 */
	private JButton runAndStopButton;
	/**
	 * 按步执行
	 */
	private JButton nextButton;

	/**
	 * 单次运行的步数
	 */
	private JTextField nextStepsTextField;

	/**
	 * 游戏运行次数
	 */
	private JTextField runTimesTextField;
	private JLabel runTimesLabel;
	/**
	 * 游戏运行速度
	 */
	private JTextField runSpaecTextField;
	private JLabel runSpaecLabel;
	/**
	 * 机器人单步时间
	 */
	private JTextField maxPlayTimeTextField;
	private JLabel maxPlayTimeLabel;

	private int x;
	private int y;
	private int width;
	private int height;

	private MainWindow main;

	private boolean isRunning = false;
	private boolean isPause = false;

	public RunToolBar(MainWindow main, int x, int y, int width, int height) {
		this.main = main;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;

		initUI();
		initEvent();
	}

	private void initEvent() {
		getReadyButton().addActionListener(new ReadyListener());
		getPauseAndResumeButton().addActionListener(
				new PauseAndResumeListener());
		getRunAndStopButton().addActionListener(new RunAndStopListener());
		getNextButton().addActionListener(new NextListener());
	}

	private void initUI() {
		setBorder(new LineBorder(Color.gray));
		setBounds(x, y, width, height);
		setLayout(null);

		add(getReadyButton());
		add(getPauseAndResumeButton());
		add(getRunAndStopButton());
		add(getNextStepsTextField());
		add(getNextButton());
		add(getRunTimesLabel());
		add(getRunTimesTextField());
		add(getRunSpaceLabel());
		add(getRunSpaceTextField());
		add(getMaxPlayTimeLabel());
		add(getMaxPlayTimeTextField());
	}

	public JButton getReadyButton() {
		if (readyButton == null) {
			readyButton = new JButton("Ready");
			readyButton.setBounds(5, 5, 100, 30);

			readyButton.setEnabled(false);
		}
		return readyButton;
	}

	public JButton getRunAndStopButton() {
		if (runAndStopButton == null) {
			runAndStopButton = new JButton("Run");
			runAndStopButton.setEnabled(false);
			runAndStopButton.setBounds(110, 5, 100, 30);
		}
		return runAndStopButton;
	}

	public JButton getPauseAndResumeButton() {
		if (pauseAndResumeButton == null) {
			pauseAndResumeButton = new JButton("Pause");
			pauseAndResumeButton.setEnabled(false);
			pauseAndResumeButton.setBounds(215, 5, 100, 30);
		}
		return pauseAndResumeButton;
	}

	public JTextField getNextStepsTextField() {
		if (nextStepsTextField == null) {
			nextStepsTextField = new JTextField("1");
			nextStepsTextField.setEnabled(false);
			nextStepsTextField.setBounds(365, 5, 100, 30);
		}
		return nextStepsTextField;
	}

	public JButton getNextButton() {
		if (nextButton == null) {
			nextButton = new JButton("Run One");
			nextButton.setEnabled(false);
			nextButton.setBounds(480, 5, 100, 30);
		}
		return nextButton;
	}

	public JTextField getRunTimesTextField() {
		if (runTimesTextField == null) {
			runTimesTextField = new JTextField("1");
			runTimesTextField.setEnabled(false);
			runTimesTextField.setBounds(70, 40, 60, 30);
		}
		return runTimesTextField;
	}

	public JLabel getRunTimesLabel() {
		if (runTimesLabel == null) {
			runTimesLabel = new JLabel("运行次数");
			runTimesLabel.setBounds(5, 40, 60, 30);
		}
		return runTimesLabel;
	}

	public JTextField getRunSpaceTextField() {
		if (runSpaecTextField == null) {
			runSpaecTextField = new JTextField("100");
			runSpaecTextField.setEnabled(false);
			runSpaecTextField.setBounds(210, 40, 60, 30);
		}
		return runSpaecTextField;
	}

	public JLabel getRunSpaceLabel() {
		if (runSpaecLabel == null) {
			runSpaecLabel = new JLabel("运行速度");
			runSpaecLabel.setBounds(145, 40, 60, 30);
		}
		return runSpaecLabel;
	}

	public JTextField getMaxPlayTimeTextField() {
		if (maxPlayTimeTextField == null) {
			maxPlayTimeTextField = new JTextField("3000");
			maxPlayTimeTextField.setEnabled(false);
			maxPlayTimeTextField.setBounds(390, 40, 60, 30);
		}
		return maxPlayTimeTextField;
	}

	public JLabel getMaxPlayTimeLabel() {
		if (maxPlayTimeLabel == null) {
			maxPlayTimeLabel = new JLabel("机器人单步时间");
			maxPlayTimeLabel.setBounds(285, 40, 100, 30);
		}
		return maxPlayTimeLabel;
	}

	public int getNextSteps() {
		try {
			return Integer.parseInt(getNextStepsTextField().getText());
		} catch (NumberFormatException e) {
			return 1;
		}
	}

	public int getRunTimes() {
		try {
			return Integer.parseInt(getRunTimesTextField().getText());
		} catch (NumberFormatException e) {
			return 1;
		}
	}

	public int getRunSpace() {
		try {
			return Integer.parseInt(getRunSpaceTextField().getText());
		} catch (NumberFormatException e) {
			return 100;
		}
	}

	public int getRobotPlayTime() {
		try {
			return Integer.parseInt(getMaxPlayTimeTextField().getText());
		} catch (NumberFormatException e) {
			return 3000;
		}
	}

	public void setRunEnable(boolean enable) {
		if (enable) {
			getReadyButton().setEnabled(true);
			getRunAndStopButton().setEnabled(false);
			getPauseAndResumeButton().setEnabled(false);

			getNextStepsTextField().setEnabled(false);
			getNextButton().setEnabled(false);

			setSettingEnable(true);
		} else {
			getReadyButton().setEnabled(false);
			getRunAndStopButton().setEnabled(false);
			getPauseAndResumeButton().setEnabled(false);

			getNextStepsTextField().setEnabled(false);
			getNextButton().setEnabled(false);

			setSettingEnable(false);
		}

	}

	public void setSettingEnable(boolean enable) {
		getRunTimesTextField().setEnabled(enable);
		getRunSpaceTextField().setEnabled(enable);
		getMaxPlayTimeTextField().setEnabled(enable);
	}

	class ReadyListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			main.launch();

			getRunAndStopButton().setEnabled(true);

			getNextStepsTextField().setEnabled(true);
			getNextButton().setEnabled(true);
		}
	}

	class RunAndStopListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (isRunning) {
				getRunAndStopButton().setText("Run");
				getPauseAndResumeButton().setText("Pause");
				isRunning = false;
				isPause = false;

				getPauseAndResumeButton().setEnabled(false);
				getNextStepsTextField().setEnabled(true);
				getNextButton().setEnabled(true);
				setSettingEnable(true);

				main.stopGame();
			} else {
				getRunAndStopButton().setText("Stop");
				isRunning = true;

				getPauseAndResumeButton().setEnabled(true);
				getNextStepsTextField().setEnabled(false);
				getNextButton().setEnabled(false);
				setSettingEnable(false);

				main.setGameRunSpace(getRunSpace());
				main.setGameMaxPlayTime(getRobotPlayTime());

				new Thread((new Runnable() {

					@Override
					public void run() {
						for (int t = 0; t < getRunTimes() && isRunning; t++) {
							main.runGame();

							while (!main.isGameEnd() && isRunning) {
								try {
									Thread.sleep(100);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}

							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
				})).start();
			}
		}
	}

	class PauseAndResumeListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (isPause) {
				getPauseAndResumeButton().setText("Pause");
				isPause = false;

				main.resumeGame();
			} else {
				getPauseAndResumeButton().setText("Resume");
				isPause = true;
				main.pauseGame();
			}
		}
	}

	class NextListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			new Thread((new Runnable() {

				@Override
				public void run() {
					getPauseAndResumeButton().setText("Pause");
					isPause = false;

					getNextButton().setEnabled(false);

					getRunAndStopButton().setEnabled(false);
					getPauseAndResumeButton().setEnabled(true);
					setSettingEnable(false);

					main.setGameRunSpace(getRunSpace());
					main.setGameMaxPlayTime(getRobotPlayTime());

					main.next(getNextSteps());

					while (!main.isGameCanNext()) {
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}

					getNextButton().setEnabled(true);

					getRunAndStopButton().setEnabled(true);
					getPauseAndResumeButton().setEnabled(false);
					setSettingEnable(true);
				}
			})).start();
		}
	}
}
