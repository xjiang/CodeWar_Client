package com.crgp.platform.xml.bean;

public class RobotBean {
	public static final String PLAYER_ROBOT_UID = "PLAYER";
	
	private String UID;
	
	public RobotBean(String UID) {
		super();
		this.UID = UID;
	}

	public String getUID() {
		return UID;
	}

	public void setUID(String uID) {
		UID = uID;
	}

	@Override
	public String toString() {
		return UID;
	}
	
	public static RobotBean getPlayerRobotBean(){
		return new RobotBean(PLAYER_ROBOT_UID);
	}
}
