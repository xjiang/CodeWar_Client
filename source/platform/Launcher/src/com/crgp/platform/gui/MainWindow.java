package com.crgp.platform.gui;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.crgp.game.BaseGame;
import com.crgp.platform.gui.panel.ConsolePanel;
import com.crgp.platform.gui.panel.RobotListPanel;
import com.crgp.platform.gui.panel.RunToolBar;
import com.crgp.platform.xml.bean.GameBean;
import com.crgp.platform.xml.bean.RobotBean;

/**
 * 主窗体
 * 
 * @author XJiang
 * 
 */
public class MainWindow extends JFrame {
	private static final long serialVersionUID = -2268430829092944845L;

	private final int ROBOT_LIST_WIDTH = 180;
	private final int RUN_TOOL_BAR_HEIGHT = 80;
	private final int CONSOLE_HIEGHT = 200;

	private RunToolBar runToolBar;
	private RobotListPanel robotListPanel;
	private ConsolePanel consolePanel;

	private JPanel gameView;

	private BaseGame game;
	private GameBean gameBean;

	public MainWindow(GameBean gameLauncher) throws Exception {
		this.gameBean = gameLauncher;

		@SuppressWarnings("unchecked")
		Class<BaseGame> c = (Class<BaseGame>) Class.forName(gameLauncher
				.getLaunchClass());
		game = c.newInstance();

		initUI();
		setResizable(false);
	}

	private void initUI() {
		setLayout(null);
		setSize(getWidth(), getHeight());
		setLocationRelativeTo(null);

		add(getToolPanel());
		add(getRobotListPanel());
		add(getGameView());
		add(getConsolePanel());

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public JPanel getGameView() {
		if (gameView == null) {
			gameView = game.createView();

			gameView.setLocation(0, RUN_TOOL_BAR_HEIGHT);
		}
		return gameView;
	}

	public RunToolBar getToolPanel() {
		if (runToolBar == null) {
			runToolBar = new RunToolBar(this, 0, 0, getWidth(),
					RUN_TOOL_BAR_HEIGHT);
		}
		return runToolBar;
	}

	public RobotListPanel getRobotListPanel() {
		if (robotListPanel == null) {
			robotListPanel = new RobotListPanel(this, getGameView().getWidth(),
					getToolPanel().getHeight(), ROBOT_LIST_WIDTH, getGameView()
							.getHeight());
		}
		return robotListPanel;
	}

	public ConsolePanel getConsolePanel() {
		if (consolePanel == null) {
			consolePanel = new ConsolePanel();
			consolePanel.setBounds(0, RUN_TOOL_BAR_HEIGHT
					+ getGameView().getHeight(), getWidth(), CONSOLE_HIEGHT);
		}
		return consolePanel;
	}

	@Override
	public int getWidth() {
		return getGameView().getWidth() + ROBOT_LIST_WIDTH;
	}

	@Override
	public int getHeight() {
		return RUN_TOOL_BAR_HEIGHT + getGameView().getHeight() + CONSOLE_HIEGHT;
	}

	/**
	 * 启动游戏
	 * 
	 * @param params
	 */
	public void launch() {
		DefaultListModel<RobotBean> listModel = (DefaultListModel<RobotBean>) getRobotListPanel()
				.getSelectList().getModel();
		for (int i = 0; i < game.getRobots().length; i++) {
			RobotBean bean = listModel.get(i);
			if (bean.getUID().equals(RobotBean.PLAYER_ROBOT_UID)) {
				game.getRobots()[i] = null;
			} else {
				game.getRobots()[i] = bean.getUID();
			}
		}

		game.launch(gameBean.getParams());
	}

	/**
	 * 设置游戏运行间隔时间
	 * 
	 * @param i
	 *            间隔时间，单位：毫秒
	 */
	public void setGameRunSpace(int runSpace) {
		game.setRunSpace(runSpace);
	}

	/**
	 * 设置游戏最大单步时间
	 * 
	 * @param i
	 *            单步时间，单位：毫秒
	 */
	public void setGameMaxPlayTime(int maxPlayTime) {
		game.setMaxPlayTime(maxPlayTime);
	}

	/**
	 * 运行游戏
	 */
	public void runGame() {
		game.run();
	}

	/**
	 * 停止行游戏
	 */
	public void stopGame() {
		game.stop();
	}

	/**
	 * 暂停行游戏
	 */
	public void pauseGame() {
		game.pause();
	}

	/**
	 * 继续游戏
	 */
	public void resumeGame() {
		game.resume();
	}

	/**
	 * 单步运行游戏
	 */
	public void next(int steps) {
		game.next(steps);
	}

	/**
	 * 判断游戏是否结束
	 * 
	 * @return
	 */
	public boolean isGameEnd() {
		return game.isEnd();
	}

	/**
	 * 判断游戏是否可以进行下一步了
	 * 
	 * @return
	 */
	public boolean isGameCanNext() {
		return game.isCanNext();
	}

	/**
	 * 获取游戏的机器人数量
	 * 
	 * @return
	 */
	public int getPlayerCount() {
		return game.playerCount();
	}

	/**
	 * 设置是否可以启动游戏
	 * 
	 * @param enabled
	 */
	public void setLauncherEnable(boolean enabled) {
		getToolPanel().setRunEnable(enabled);
	}
}
